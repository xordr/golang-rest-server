FROM golang:1.9

COPY . /go/src/main/

WORKDIR /go/src/main

RUN go get -d -v
RUN go install -v

CMD ["main"]