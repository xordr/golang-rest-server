package main

import (
	"encoding/json"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"net"
	"strconv"
)

type Device struct {
	DeviceBasic
	HardwareAddress string   `json:"hardwareAddress,omitempty"`
	MTU             string   `json:"mtu,omitempty"`
	Flags           string   `json:"flags,omitempty"`
}

type DeviceBasic struct {
	ID         string   `json:"id,omitempty"`
	DeviceName string   `json:"deviceName,omitempty"`
}

var devices []Device
var devicesBasic []DeviceBasic

func GetNetworkDevicesListHandler(w http.ResponseWriter, req *http.Request) {

	SetHeaders(w)

	json.NewEncoder(w).Encode(devicesBasic)
}

func GetCertainNetworkDeviceHandler(w http.ResponseWriter, req *http.Request) {

	SetHeaders(w)

	params := mux.Vars(req)
	for _, device := range devices {
		if device.ID == params["id"] {
			json.NewEncoder(w).Encode(device)
			return
		}
	}

	json.NewEncoder(w).Encode(&Device{})
}

func InitDevices() {
	interfaces, err := net.Interfaces()

	if (err != nil) {
		println("ERROR")
	} else {
		for _, device := range interfaces {
			devices = append(devices, Device{DeviceBasic: DeviceBasic{ID: strconv.Itoa(device.Index), DeviceName: device.Name}, HardwareAddress: device.HardwareAddr.String(), MTU: strconv.Itoa(device.MTU), Flags: device.Flags.String()})
			devicesBasic = append(devicesBasic, DeviceBasic{ID: strconv.Itoa(device.Index), DeviceName: device.Name})
		}
	}
}

func SetHeaders(w http.ResponseWriter) {
	w.Header().Set("Content-type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
}

func main() {

	InitDevices()

	router := mux.NewRouter()
	router.HandleFunc("/devices", GetNetworkDevicesListHandler).Methods("GET")
	router.HandleFunc("/devices/{id}", GetCertainNetworkDeviceHandler).Methods("GET")

	log.Fatal(http.ListenAndServe(":8080", router))
}