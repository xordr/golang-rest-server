package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"net"
	"strconv"
	"strings"
	"io/ioutil"
	"github.com/gorilla/mux"
	"encoding/json"
)

func TestGetNetworkDevicesListHandler(t *testing.T) {

	InitDevices()

	req, err := http.NewRequest("GET", "/devices", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetNetworkDevicesListHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	devices, err1 := net.Interfaces()
	if err1 != nil {
		t.Fatal(err1)
	}

	expected := `[`

	for i := 0; i < len(devices); i++ {
		expected += `{"id":"` + strconv.Itoa(devices[i].Index) + `","deviceName":"` + devices[i].Name + `"},`
	}

	expected = strings.TrimRight(expected, ",")
	expected += "]\n"

	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}

}

func TestGetCertainNetworkDeviceHandler(t *testing.T) {
	InitDevices()

	r := mux.NewRouter()
	r.HandleFunc("/devices/{id}", GetCertainNetworkDeviceHandler)

	server := httptest.NewServer(r)
	defer server.Close()

	devices, _ := net.Interfaces()

	for i := 0; i < len(devices); i++ {
		url := "http://localhost:8080/devices/" + strconv.Itoa(devices[i].Index)
		resp, err := http.Get(url)
		if err != nil {
			t.Fatal(err)
		}

		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatal(err)
		}

		var dev Device
		json.Unmarshal(bodyBytes, &dev)

		if devices[i].Name != dev.DeviceName {
			t.Errorf("handler returned unexpected value: got %v want %v",
				dev.DeviceName, devices[i].Name)
		}

		if devices[i].Flags.String() != string(dev.Flags) {
			t.Errorf("handler returned unexpected value: got %v want %v",
				string(dev.Flags), devices[i].Flags.String())
		}

		if strconv.Itoa(devices[i].Index) != dev.ID {
			t.Errorf("handler returned unexpected value: got %v want %v",
				dev.ID, string(devices[i].Index))
		}

		if devices[i].HardwareAddr.String() != dev.HardwareAddress {
			t.Errorf("handler returned unexpected value: got %v want %v",
				dev.HardwareAddress, devices[i].HardwareAddr.String())
		}

		if strconv.Itoa(devices[i].MTU) != dev.MTU {
			t.Errorf("handler returned unexpected value: got %v want %v",
				dev.MTU, strconv.Itoa(devices[i].MTU))
		}
	}

}